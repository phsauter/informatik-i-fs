# Informatik I FS
Diese Zusammenfassung gehört zu den Vorlesungen "Informatik I" von Malte Schwerhoff.
Wenn ihr irgendwelche Änderungsvorschläge habt (oder wenn ihr Fehler gefunden habt) könnt ihr mich entweder per Mail erreichen oder ihr könnt hier ein "Issue" eröffnen.
[phsauter@student.ethz.ch](phsauter@student.ethz.ch)
## Disclaimer
Das Layout wurden der Zusammenfassung von Theo von Arx (HS16-FS17; Analysis) entnommen.
Einige Befehle etc. wurden von der Zusammenfassung von Silvano Cortesi (Analysis) übernommen.
Der Inhalt basier grob auf der Zusammenfassung von Manuel Eggimann (HS13).
**Es ist gut möglich, dass die Zusammenfassung inhaltliche Fehler aufweist.**
